############
KIALI_REPO = https://kiali.org/helm-charts 
KEDA_REPO = https://kedacore.github.io/chartss 
PROMETHEUS_REPO = https://prometheus-community.github.io/helm-charts
ARGO_REPO = https://argoproj.github.io/argo-helm 
NGINX_REPO = https://kubernetes.github.io/ingress-nginx
SEALED_SECRETS_REPO = https://bitnami-labs.github.io/sealed-secrets
CHAOS_REPO = https://charts.chaos-mesh.org 
KYVERNO_REPO = https://kyverno.github.io/kyverno/
GRAFANA_REPO = https://grafana.github.io/helm-charts
GITLAB_REPO = https://charts.gitlab.io
FLAGGER_REPO = https://flagger.app

all: help

help:
	@echo ----------------------------------
	@echo DEPLOYMENT COMMANDS
	@echo ----------------------------------
	@echo make istio-install 
	@echo make istio-delete 	
	@echo make nginx-install 	
	@echo make nginx-delete
	@echo make argocd-install 	
	@echo make argocd-delete	
	@echo make keda-install 	
	@echo make keda-delete	
	@echo make my-app-dev-install 	
	@echo make my-app-dev-delete	
	@echo make my-app-prod-install 	
	@echo make my-app-prod-delete	
	@echo make prometheus-install
	@echo make prometheus-delete
	@echo make sealed-secrets-install
	@echo make sealed-secrets-delete
	@echo make chaos-mesh-install
	@echo make chaos-mesh-delete
	@echo make kyverno-install
	@echo make kyverno-delete
	@echo make grafana-loki-install
	@echo make grafana-loki-delete
	@echo make gitlab-runner-install
	@echo make gitlab-runner-delete
	@echo make argo-rollouts-install
	@echo make argo-rollouts-delete
	@echo make argo-workflows-install
	@echo make argo-workflows-delete	
	@echo make flagger-install
	@echo make flagger-delete

istio-install:
	@kubectl create namespace istio-system | true
	@istioctl install --set profile=default -y
	@helm repo add  kiali-server $(KIALI_REPO) | true
	@helm repo update
	@helm install kiali kiali-server/kiali-server --namespace istio-system  --values ./charts/kiali/values.yml	
	@kubectl -n istio-system patch service istio-ingressgateway --patch-file ./charts/istio/patch.yml
	@kubectl apply -f ./charts/prometheus/service-monitor/prometheus-operator.yaml -n istio-system
	@kubectl apply -f ./charts/istio/jaeger.yaml -n istio-system
		
istio-delete:	
	@istioctl manifest generate --set profile=default | kubectl delete --ignore-not-found=true -f -
	@sleep 5s
	@helm uninstall kiali -n istio-system
	@kubectl delete -f ./charts/prometheus/service-monitor/prometheus-operator.yaml -n istio-system
	@kubectl delete -f ./charts/istio/jaeger.yaml -n istio-system

nginx-install:
	@helm repo add ingress-nginx $(NGINX_REPO) | true
	@helm repo update
	@helm install nginx-ingress ingress-nginx/ingress-nginx -n ingress-nginx --values ./charts/nginx-ingress/values.yml --create-namespace	

nginx-delete:
	@helm uninstall nginx-ingress -n ingress-nginx

argocd-install:
	@helm repo add argo $(ARGO_REPO) | true
	@helm repo update
	@helm install argocd argo/argo-cd -n argocd --values ./charts/argocd/values.yml --create-namespace 

argocd-delete:
	@helm uninstall argocd -n argocd

keda-install:
	@helm repo add kedacore $(KEDA_REPO) | true
	@helm repo update
	@helm install keda kedacore/keda --namespace keda --create-namespace

keda-delete:
	@helm uninstall keda -n keda

my-app-dev-install:
	@helm install my-app-dev ./charts/my-app --values ./charts/my-app/dev.yml -n dev --create-namespace

my-app-dev-delete:
	@helm uninstall my-app-dev -n dev

my-app-prod-install:
	@helm install my-app-prod ./charts/my-app --values ./charts/my-app/prod.yml -n prod --create-namespace

my-app-prod-delete:
	@helm uninstall my-app-prod -n prod

prometheus-install:
	@helm repo add prometheus-community $(PROMETHEUS_REPO) | true
	@helm repo update
	@helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring --create-namespace --values ./charts/prometheus/values.yml
	@kubectl create -f ./charts/prometheus/dashboards/istio-dashboards.yml

prometheus-delete:
	@helm uninstall monitoring -n monitoring
	@kubectl delete -f ./charts/prometheus/dashboards/istio-dashboards.yml

sealed-secrets-install:
	@helm repo add sealed-secrets $(SEALED_SECRETS_REPO) | true
	@helm repo update
	@helm install sealed-secrets sealed-secrets/sealed-secrets -n kube-system --values ./charts/sealed-secrets/values.yml

sealed-secrets-delete:
	@helm uninstall sealed-secrets -n kube-system 

chaos-mesh-install:
	@helm repo add chaos-mesh $(CHAOS_REPO) | true
	@helm repo update
	@helm install chaos-mesh chaos-mesh/chaos-mesh -n chaos-mesh --create-namespace --values ./charts/chaos-mesh/values.yml

chaos-mesh-delete:
	@helm uninstall chaos-mesh -n chaos-mesh

kyverno-install:
	@helm repo add kyverno $(KYVERNO_REPO) | true
	@helm repo update
	@helm install kyverno kyverno/kyverno -n kyverno --create-namespace

kyverno-delete:
	@helm uninstall kyverno -n kyverno

grafana-loki-install:
	@helm repo add grafana $(GRAFANA_REPO) | true
	@helm repo update
	@helm install loki grafana/loki-stack -n loki --create-namespace --values ./charts/loki/values.yml

grafana-loki-delete:
	@helm uninstall loki -n loki

gitlab-runner-install:
	@helm repo add gitlab $(GITLAB_REPO) | true
	@helm repo update
	@helm install gitlab-runner gitlab/gitlab-runner -n gitlab-runner --create-namespace --values ./charts/gitlab-runner/values.yml

gitlab-runner-delete:
	@helm uninstall gitlab-runner -n gitlab-runner 

argo-rollouts-install:
	@helm repo add argo $(ARGO_REPO) | true
	@helm repo update
	@helm install argo-rollouts argo/argo-rollouts -n argo-rollouts --values ./charts/argo-rollouts/values.yml --create-namespace 

argo-rollouts-delete:
	@helm uninstall argo-rollouts -n argo-rollouts

argo-workflows-install:
	@helm repo add argo $(ARGO_REPO) | true
	@helm repo update
	@helm install argo-workflows argo/argo-workflows -n argo-workflows --values ./charts/argo-workflows/values.yml --create-namespace 

argo-workflows-delete:
	@helm uninstall argo-workflows -n argo-workflows

flagger-install:
	@helm repo add flagger $(FLAGGER_REPO) | true
	@helm repo update
	@helm install flagger flagger/flagger -n ingress-nginx --values ./charts/flagger/values.yml
	
flagger-delete:
	@helm uninstall flagger -n ingress-nginx 