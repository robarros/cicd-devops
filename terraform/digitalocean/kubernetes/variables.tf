variable "pool_size" {
  type    = string
  default = "s-4vcpu-8gb"
}

variable "ingress_nginx" {
  type    = bool
  default = false
}

variable "min_nodes" {
  type    = number
  default = 1
}

variable "max_nodes" {
  type    = number
  default = 2
}

variable "do_token" {
  type    = string
  default = ""
}

# size droplet
# s-1vcpu-2gb
# s-1vcpu-3gb
# s-2vcpu-2gb
# s-2vcpu-4gb
# s-3vcpu-1gb
# s-4vcpu-8gb


# Regions 
# nyc1  nyc3
# sfo3
# ams3
# sgp1
# lon1
# fra1 
# blr1
# tor1
