terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.9.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_kubernetes_versions" "version" {}

resource "digitalocean_kubernetes_cluster" "k8s-lab" {
  name    = "k8s-lab"
  region  = "nyc1"
  version = data.digitalocean_kubernetes_versions.version.latest_version
  node_pool {
    name       = "poll-labs"
    size       = var.pool_size
    auto_scale = false
    node_count = 1
    min_nodes  = var.min_nodes
    max_nodes  = var.max_nodes
  }
}

resource "null_resource" "kubeconfig" {
  provisioner "local-exec" {
    command = "echo '${digitalocean_kubernetes_cluster.k8s-lab.kube_config[0].raw_config}' | tee kubeconfig.yaml"
  }
  depends_on = [
    digitalocean_kubernetes_cluster.k8s-lab,
  ]
}

resource "null_resource" "ingress-nginx" {
  count = var.ingress_nginx == true ? 1 : 0
  provisioner "local-exec" {
    command = "KUBECONFIG=$PWD/kubeconfig.yaml kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.46.0/deploy/static/provider/do/deploy.yaml"
  }
  depends_on = [
    null_resource.kubeconfig,
  ]
}

resource "null_resource" "destroy-kubeconfig" {
  provisioner "local-exec" {
    when    = destroy
    command = "rm -rf $PWD/kubeconfig.yaml"
  }
}
