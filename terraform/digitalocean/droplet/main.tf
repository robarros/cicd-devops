terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.9.0"
    }
  }
}


provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "ssh" {
  name = var.name_key
}

resource "digitalocean_droplet" "srv" {
  image              = "ubuntu-20-04-x64"
  name               = "srv"
  count              = 1
  region             = var.region
  size               = var.size
  ssh_keys           = [data.digitalocean_ssh_key.ssh.id]
  private_networking = true

  provisioner "remote-exec" {
    inline = ["sudo apt update", "sudo apt install python -y", "echo Done!"]

    connection {
      host        = self.ipv4_address
      type        = "ssh"
      user        = "root"
      private_key = file("~/.ssh/id_rsa")
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.ipv4_address},' ../../../ansible/fail2ban/playbook.yml"
  }
}


output "instance_ipv4_addr" {
  value = digitalocean_droplet.srv[*].ipv4_address
}

