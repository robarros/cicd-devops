#! /bin/bash

npm install @semantic-release/commit-analyzer @semantic-release/release-notes-generator @semantic-release/changelog \
@semantic-release/npm @semantic-release/gitlab @semantic-release/git --save-dev

npm install --save-dev husky @commitlint/cli @commitlint/config-conventional

npm install commitizen -g

commitizen init cz-conventional-changelog --save-dev --save-exact
