## [1.1.2](https://github.com/robarros/cicd-devops/compare/1.1.1...1.1.2) (2021-06-14)


### Bug Fixes

* **add new fix:** ssssssssss ([8451819](https://github.com/robarros/cicd-devops/commit/8451819f64c5ecdaad5851d3914019d325e58bc1))
* **add new version:** add new version ([ff00d03](https://github.com/robarros/cicd-devops/commit/ff00d038f87dcc99494722e070cc38170e0d62ad))
* **fix:** xxxxxxxxxxx ([bf85378](https://github.com/robarros/cicd-devops/commit/bf8537887736e4368b829c8ddc17ec3262257a7d))
* **fix new bugs:** change npm version ([adc3e78](https://github.com/robarros/cicd-devops/commit/adc3e7831d745a4d001f17438b4efe5856c4a62f))
* **fix script:** fix script get-tag ([52f33ff](https://github.com/robarros/cicd-devops/commit/52f33ff452a8a04c133835df323481038cb834a5))

## [1.1.1](https://github.com/robarros/cicd-devops/compare/1.1.0...1.1.1) (2021-06-11)


### Bug Fixes

* **add stage ci/cd:** add stage deploy and fake tag ([83aadee](https://github.com/robarros/cicd-devops/commit/83aadeedf2615e8d9b4828b58c1767b36863ce5b))

# [1.1.0](https://github.com/robarros/cicd-devops/compare/1.0.2...1.1.0) (2021-06-10)


### Bug Fixes

* **change image node:** change image node to node:13-slim ([d58ca4a](https://github.com/robarros/cicd-devops/commit/d58ca4aedd5b45921bac9b383613acc3f4aa5d50))
* **fix image node:** change to image node:13 ([fa8a0c9](https://github.com/robarros/cicd-devops/commit/fa8a0c9ad6c97756cc52bab103b83bfabb599ebe))


### Features

* **add new feature:** add new option ([3f53f7c](https://github.com/robarros/cicd-devops/commit/3f53f7cdbcfa1cd42672a0341370c5009a2f03a8))

## [1.0.2](https://github.com/robarros/cicd-devops/compare/1.0.1...1.0.2) (2021-06-10)


### Bug Fixes

* **add novo fix:** alterado gitlabci.yml ([32825c0](https://github.com/robarros/cicd-devops/commit/32825c081754e484c8b8c347611f3c3b14e7bc88))

## [1.0.1](https://github.com/robarros/cicd-devops/compare/v1.0.0...1.0.1) (2021-06-10)


### Bug Fixes

* **new bug fix:** add script ([6ce1870](https://github.com/robarros/cicd-devops/commit/6ce1870c5f88089660d070e0db891797b80b2858))

# 1.0.0 (2021-06-10)


### Bug Fixes

* **sssssssssssssssss:** 11111111111111111111 ([217b630](https://github.com/robarros/cicd-devops/commit/217b630ae5e1cf59bb44a03b9da306c2ecf9df3a))
* **wwwwwwwwwww:** qqqqqqqqqqqqqqq ([8e135b2](https://github.com/robarros/cicd-devops/commit/8e135b25b268c363ce2247216b299202655a9d28))
